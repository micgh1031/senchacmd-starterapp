/**
 * This view was originally a list of people, warren changed it to an audit log list
 */
Ext.define('SenchaStarterApp2.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'SenchaStarterApp2.store.AuditLog'
    ],

    title: 'AuditLog',

    store: {
        type: 'auditlog'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', width: 100 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Phone', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
