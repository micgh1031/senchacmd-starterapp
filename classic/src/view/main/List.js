/*
 * SenchaStarterApp2 : This ExtJS 6 App talks to a sample Direct deta set.
 * see store/AuditLog.js
 * 
 * References to TutorialApp2 are out of date.
 *
 */
Ext.define('SenchaStarterApp2.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    requires: [
        'SenchaStarterApp2.store.AuditLog'
    ],

    title: 'Audit Log Search Results',

    style: 'x',

    store: {
        type: 'AuditLog'
    },


    columns: [
        // text:"x" dataIndex:"x" flex:0|1
        { text: "Access Time",          dataIndex: "accesstime", width:220 }, //  "2014-07-14T15:12:11",
        {text: "Patient",               dataIndex: "patientfullname",  width:220  }, // "WAGMAMA^JULIE",
        {text: "Accessed Data",         dataIndex: "accesseddata", width:300 },// "STUDY 1.2.124.113540.0.0.3.629",

        { text: "Study Instance UID",   dataIndex: "studyinstanceuid", width:120 }, // "1.2.124.113540.0.0.3.629"

        { text: "User Name",            dataIndex: "username"}, //  "ADMIN",
        { text: "Access Type",          dataIndex: "accesstype"}, //  "VIEW",
        { text: "Status Order",         dataIndex: "statusorder"}, //  30,
        { text: "Study Date/Time",      dataIndex: "studydatetime"}, //  "2014-07-04T15:45:00",
        { text: "Accession Number",     dataIndex: "accessionnumber"}, //  "RAM629",
        {text: "Institution",           dataIndex: "institutionname"}, //  "ACTFAC01",
        {text: "Patient ID",            dataIndex: "patientid"}, // "879",
        {text: "Issuer of Patient ID",  dataIndex: "issuerofpatientid"}, // "PETER",
        {text: "Status",                dataIndex: "status"}, // "SCHEDULED",
        {text: "Modified Data",         dataIndex: "modifieddata"}
//        {text: "User ID", dataIndex: "userid"}, // integer primary id,
//        {text: "Identity", dataIndex: "_fbidentity"},
//        {text: "Doc Type", "_document_type"},
//        {text: "Doc Rev", "_document_rev" },
//        {text: "Doc Origin", "_document_origin": "FBIMPORT"


    ],

    listeners: {
        select: 'onItemSelected'
    }
});
