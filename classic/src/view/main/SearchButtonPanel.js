/**
 * Created by wpostma on 22-Jul-15.
 */
Ext.define('SenchaStarterApp2.view.main.SearchButtonPanel', {
    extend: 'Ext.Container',

        xtype: 'search-button-panel',

        border: false,

        style: {
            'margin-top':    '20px',
            'margin-bottom': '20px'
        },

        requires: [
            'Ext.form.*',
            'Ext.button.*'

        ],

        items: [
            {
                xtype : "button",
                text : "Search Log",
                width: 100,
                height: 30


            }
        ]
    });