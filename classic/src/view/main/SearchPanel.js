/* Audit Log Search controls panel
 */

Ext.define('SenchaStarterApp2.view.main.SearchPanel', {
    extend: 'Ext.panel.Panel',

    title: 'Search',

    xtype: 'search-panel',

    requires: [
        'Ext.form.*',
        'Ext.button.*'

    ],

    labelPad: 0,

   /* layout: "column", */

    /* bodyPadding: "25px", */

    padding: "3px",

    columns: 2,
    items: [
        {
            xtype : "radiogroup",
            fieldLabel : "Search Type",
            vertical:true,
            columns: 4,
            width: 900,
            items : [
                {boxLabel:"Study Access",name:"type1"},
                {boxLabel:"Patient Access",name:"type2"},
                {boxLabel:"User",name:"type3"},
                {boxLabel:"Print",name:"type4"}
            ]
        },
        {
            xtype :     "textfield",
            fieldLabel : "Patient Name",
            width:       500
        },
        {
            xtype : "datefield",
            fieldLabel : "Date Start"
        },
        {
            xtype : "datefield",
            fieldLabel : "Date End"
        }
        /*

        {
            xtype : "textfield",
            fieldLabel : "Blog"
        },
        {
            xtype : "numberfield",
            fieldLabel : "Years of experience",
            minValue : 5,
            maxValue : 15
        },
        {
            xtype : "textarea",
            fieldLabel : "Address",
            width:600
        }
        */
    ]
});