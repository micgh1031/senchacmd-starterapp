Ext.define('SenchaStarterApp2.store.AuditLog', {
    extend: 'Ext.data.Store',

    alias: 'store.AuditLog',

    fields: [
        'name', 'email', 'phone'
    ],

    data: { items: [

        {
            "_docid": "STUDYACCESS.1473",
            "studyinstanceuid": "1.2.124.113540.0.0.3.629",
            "accesstime": "2014-07-14T15:12:11",
            "username": "ADMIN",
            "accesstype": "VIEW",
            "statusorder": 30,
            "studydatetime": "2014-07-04T15:45:00",
            "accessionnumber": "RAM629",
            "institutionname": "ACTFAC01",
            "patientid": "879",
            "issuerofpatientid": "PETER",
            "patientfullname": "WAGMAMA^JULIE",
            "status": "SCHEDULED",
            "userid": 1,
            "_fbidentity": 1473,
            "accesseddata": "STUDY 1.2.124.113540.0.0.3.629",
            "modifieddata": "",
            "_document_type": "STUDYACCESS",
            "_document_rev": "2",
            "_document_origin": "FBIMPORT"
        }


    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
